import sqlite3
import csv
import logging
from datetime import datetime

logging.basicConfig(format='%(asctime)s : %(levelname)s :  %(message)s',filename=datetime.now().strftime('%Hh%M_le_%d_%m_%Y.log') , datefmt='%d/%m/%Y %I:%M:%S',level=logging.DEBUG)
def creationBDD(nom):
    conn = sqlite3.connect(nom + '.sqlite3')
    cursor = conn.cursor()
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS automobile(
        adresse_titulaire TEXT,
        nom_titulaire TEXT,
        prenom_titulaire TEXT,
        immatriculation TEXT,
        date_immatriculation TEXT,
        vin INTEGER PRIMARY KEY,
        marque_vehicule TEXT,
        denomination_commerciale TEXT,
        couleur_vehicule TEXT,
        carosserie TEXT,
        categorie TEXT,
        cylindree INTEGER,
        energie INTEGER,
        nb_places INTEGER,
        poids_vehicule INTEGER,
        puissance INTEGER,
        type TEXT,
        variante TEXT,
        version INTEGER
    );
    """)
    conn.commit()
    return conn


def ouverture(connecteur):
    cursor = connecteur.cursor()
    with open('fichier.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=";")
        for row in reader:
            cursor.execute("""SELECT vin FROM automobile where vin=?""",(row[5],))
            requete = cursor.fetchall()
            if(len(requete)==0):
                insertion(connecteur,row)
            else:
                update(connecteur,row)
        
def insertion(connecteur,row):
    cursor = connecteur.cursor()
    cursor.execute("""
                INSERT INTO automobile(adresse_titulaire, nom_titulaire , prenom_titulaire, immatriculation, date_immatriculation, vin, marque_vehicule, denomination_commerciale, couleur_vehicule, carosserie, categorie, cylindree, energie, nb_places, poids_vehicule, puissance, type, variante, version) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);""", row)
    connecteur.commit()
    logging.info("Objet " + str(row[5]) + " inséré")

def update(connecteur,row):
    cursor = connecteur.cursor()
    vin = row.pop(5)
    row.append(vin)
    cursor.execute("""UPDATE automobile set adresse_titulaire=?,nom_titulaire=?,prenom_titulaire=?,immatriculation=?,date_immatriculation=?,marque_vehicule=?,denomination_commerciale=?,couleur_vehicule=?,carosserie=?,categorie=?,cylindree=?,energie=?,nb_places=?,poids_vehicule=?,puissance=?,type=?,variante=?,version=? WHERE vin=?;""",row)
    logging.info("Objet " + str(row[18]) + " modifié")
conn = creationBDD("automobile")
ouverture(conn)
conn.close()