import unittest
import sqlite3
import os
from prog_import import creationBDD
from prog_import import insertion

class ImportTest(unittest.TestCase):
    row=["3822 Omar Square Suite 257 Port Emily, OK 43251","Smith","Jerome","OVC-568","03/05/2012",0000000000000000000,"Williams Inc","Enhanced well-modulated moderator","LightGoldenRodYellow",45-1743376,34-7904216,3462,37578077,32,3827,110,"Inc",92-3625175,7926648]
    def setUp(self):
        self.connection = creationBDD("test")
    def test_upsert(self):
        cursor = self.connection.cursor()
        cursor.execute("""SELECT * from automobile;""")
        nbligne = cursor.fetchall()
        self.assertEqual(0,len(nbligne))
        insertion(self.connection, ImportTest.row)
        cursor.execute("""SELECT * from automobile;""")
        nbligne = cursor.fetchall()
        self.assertEqual(1,len(nbligne))
        
    def tearDown(self):
        self.connection.close()
        os.remove("test.sqlite3")

    
        